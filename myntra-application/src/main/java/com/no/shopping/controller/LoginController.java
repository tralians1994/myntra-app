package com.no.shopping.controller;

import com.no.shopping.dto.request.LoginRequest;
import com.no.shopping.dto.response.UserDetailsResponse;
import com.no.shopping.service.LoginService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    private LoginService service;

    LoginController(LoginService service) {
        this.service = service;
    }

    public ResponseEntity<UserDetailsResponse> postLogin(LoginRequest request) {
        return ResponseEntity.ok(service.login(request));
    }
}
