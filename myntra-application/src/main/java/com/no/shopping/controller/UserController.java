package com.no.shopping.controller;

import com.no.shopping.dto.request.CreateUserRequest;
import com.no.shopping.dto.response.CreateUserResponse;
import com.no.shopping.dto.response.UserDetailsResponse;
import com.no.shopping.service.UserService;
import com.no.shopping.validation.CreateUserRequestValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
public class UserController {

    private UserService service;

    UserController(UserService service) {
        this.service = service;
    }

    @PostMapping(value = "/user", consumes = "application/json", produces = "application/json")
    ResponseEntity<CreateUserResponse> postUser(
            @RequestHeader(value = "X-Trace-ID") String traceId,
            @Valid @RequestBody CreateUserRequest request) {
        CreateUserRequestValidator.validate(request);
        CreateUserResponse response = service.createUser(request);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping(value = "/user/{userName}", produces = "application/json")
    ResponseEntity<UserDetailsResponse> getUser(
            @RequestHeader(value = "X-Trace-ID") String traceId,
            @PathVariable("userName") @NotBlank @Valid String userName) {
        UserDetailsResponse response = service.getUser(userName);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
