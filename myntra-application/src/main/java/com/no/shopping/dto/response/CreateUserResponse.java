package com.no.shopping.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUserResponse {

    private Long id;

    private String userName;
}
