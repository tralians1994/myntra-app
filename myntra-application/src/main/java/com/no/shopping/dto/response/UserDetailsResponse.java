package com.no.shopping.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDetailsResponse {

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private String contactNo;

    private String countryCode;

    private String userName;
}
