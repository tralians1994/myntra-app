package com.no.shopping.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

@Getter
@Setter
public class CreateUserRequest {

    @NotBlank(message = "firstName cannot be blank")
    private String firstName;

    @NotBlank(message = "lastName cannot be blank")
    private String lastName;

    @Email(message = "invalid email")
    private String email;

    @Size(min = 6, message = "password should at least have 6 characters")
    private String password;

    @Size(min = 6, message = "password should at least have 6 characters")
    private String confirmPassword;

    private String contactNo;

    @Size(min = 2, max = 2)
    private String countryCode;

    @NotBlank(message = "userName cannot be blank")
    private String userName;
}
