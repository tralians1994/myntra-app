package com.no.shopping.advice;

import com.no.shopping.exception.GeneralError;
import com.no.shopping.exception.MyntraException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalControllerAdvice {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<GeneralError> handleError(Exception ex) {
        GeneralError error = null;
        if (ex instanceof MyntraException) {
            MyntraException exception = (MyntraException) ex;
            error = new GeneralError(exception.getStatus(), exception.getErrorCode(), exception.getErrorMsg());
        } else {
            error = new GeneralError(500, "", ex.getMessage());
        }
        return ResponseEntity.status(error.getStatus()).body(error);
    }
}
