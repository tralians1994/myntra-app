package com.no.shopping.service;

import com.no.shopping.dto.request.LoginRequest;
import com.no.shopping.dto.response.UserDetailsResponse;

public interface LoginService {

    UserDetailsResponse login(LoginRequest request);
}
