package com.no.shopping.service.impl;

import com.no.shopping.dto.request.LoginRequest;
import com.no.shopping.dto.response.UserDetailsResponse;
import com.no.shopping.exception.MyntraException;
import com.no.shopping.model.User;
import com.no.shopping.repo.UserRepo;
import com.no.shopping.service.LoginService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    private UserRepo repo;

    LoginServiceImpl(UserRepo repo) {
        this.repo = repo;
    }

    @Override
    public UserDetailsResponse login(LoginRequest request) {
        User user = repo.findByUserName(request.getUsername())
                .orElseThrow(() -> new MyntraException(HttpStatus.NOT_FOUND.value(), "User not found"));

        if (!user.getPassword().equals(request.getPassword())) {
            throw new MyntraException(HttpStatus.BAD_REQUEST.value(), "Incorrect Password");
        }

        UserDetailsResponse response = new UserDetailsResponse();
        BeanUtils.copyProperties(user, response);
        return response;
    }
}
