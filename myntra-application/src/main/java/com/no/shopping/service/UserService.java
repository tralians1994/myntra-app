package com.no.shopping.service;

import com.no.shopping.dto.request.CreateUserRequest;
import com.no.shopping.dto.response.CreateUserResponse;
import com.no.shopping.dto.response.UserDetailsResponse;

public interface UserService {

    CreateUserResponse createUser(CreateUserRequest request);

    UserDetailsResponse getUser(String userName);
}
