package com.no.shopping.service.impl;

import com.no.shopping.converter.UserConverter;
import com.no.shopping.dto.request.CreateUserRequest;
import com.no.shopping.dto.response.CreateUserResponse;
import com.no.shopping.dto.response.UserDetailsResponse;
import com.no.shopping.exception.MyntraException;
import com.no.shopping.model.User;
import com.no.shopping.repo.UserRepo;
import com.no.shopping.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepo repo;

    UserServiceImpl(UserRepo repo) {
        this.repo = repo;
    }

    public CreateUserResponse createUser(CreateUserRequest request) {
        User user = UserConverter.convert(request);

        if (repo.findByUserName(request.getUserName()).isPresent()) {
            throw new MyntraException(HttpStatus.CONFLICT.value(), "User Already Exist");
        }
        repo.save(user);
        return UserConverter.convert(user);
    }

    @Override
    public UserDetailsResponse getUser(String userName) {
        User user = repo.findByUserName(userName)
                .orElseThrow(() -> new MyntraException(HttpStatus.NOT_FOUND.value(), "User not found"));
        UserDetailsResponse response = new UserDetailsResponse();
        BeanUtils.copyProperties(user, response);
        return response;
    }
}
