package com.no.shopping.converter;

import com.fasterxml.jackson.databind.util.BeanUtil;
import com.no.shopping.dto.request.CreateUserRequest;
import com.no.shopping.dto.response.CreateUserResponse;
import com.no.shopping.model.User;
import org.springframework.beans.BeanUtils;

public final class UserConverter {

    public static User convert(CreateUserRequest request) {
        User user = new User();
        BeanUtils.copyProperties(request, user);
        return user;
    }

    public static CreateUserResponse convert(User user) {
        CreateUserResponse response = new CreateUserResponse();
        return response;
    }
}
