package com.no.shopping.model;

import lombok.Data;

import javax.persistence.*;

@Table(name = "User")
@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private String contactNo;

    private String countryCode;

    private String userName;
}
