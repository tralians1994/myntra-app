package com.no.shopping.validation;

import com.no.shopping.dto.request.CreateUserRequest;
import com.no.shopping.exception.MyntraException;
import org.springframework.http.HttpStatus;

public final class CreateUserRequestValidator {

    public static final void validate(CreateUserRequest request) {
        if (!request.getPassword().equals(request.getConfirmPassword())) {
            throw new MyntraException(HttpStatus.BAD_REQUEST.value(), "Password does not match");
        }
    }
}
