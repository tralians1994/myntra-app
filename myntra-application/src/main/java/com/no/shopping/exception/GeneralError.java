package com.no.shopping.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GeneralError {

    private int status;

    private String errorCode;

    private String errorMessage;
}
