package com.no.shopping.exception;

import lombok.Data;

@Data
public class MyntraException extends RuntimeException{

    private int status;

    private String errorMsg;

    private String errorCode;

    public MyntraException() {

    }

    public MyntraException(int status, String message) {
        super();
        this.status = status;
        this.errorMsg = message;
    }

    public MyntraException(int status, String message, String errorCode) {
        this(status, message);
        this.errorCode = errorCode;
    }
}
